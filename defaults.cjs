function defaults(obj, defaultProps) {
    if (typeof obj === 'object') {
        let answer = {};
        for (let property in defaultProps) {
            if(obj[property] === undefined ){
                obj[property] = defaultProps[property];
            }
        }
        answer = obj;
        return answer;
    } else {
        return [];
    }
}

module.exports = defaults;