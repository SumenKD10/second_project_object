function mapObject(objectName, cb) {
    if (typeof cb != 'function') {
        return [];
    }
    if (typeof objectName === 'object') {
        let answer = [];
        for (let property in objectName) {
            objectName[property] = cb(objectName[property]);
        }
        return objectName;
    } else {
        return [];
    }
}

module.exports = mapObject;