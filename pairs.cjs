function pairs(objectName) {
    if (typeof objectName === 'object') {
        let answer = [];
        for (let property in objectName) {
            answer.push([property,objectName[property]])
        }
        return answer;
    } else {
        return [];
    }
}

module.exports = pairs;