function keys(objectName) {
    if (typeof objectName === 'object') {
        let answer = [];
        for (let property in objectName) {
            answer.push(property);
        }
        return answer;
    } else {
        return [];
    }
}

module.exports = keys;