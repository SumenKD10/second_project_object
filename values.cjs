function values(objectName) {
    if (typeof objectName === 'object') {
        let answer = [];
        for (let property in objectName) {
            if (typeof objectName[property] != 'function') {
                answer.push(objectName[property]);
            }
        }
        return answer;
    } else {
        return [];
    }
}

module.exports = values;