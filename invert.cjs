function invert(objectName) {
    if (typeof objectName === 'object') {
        let answer = {};
        for (let property in objectName) {
            answer[objectName[property]] = property;
        }
        return answer;
    } else {
        return [];
    }
}

module.exports = invert;