let mapObject = require('../mapObject.cjs');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const callBack = function (variable) {
    return variable + 5;
}
let result = mapObject(testObject, callBack);
console.log(result);